Photos From Here Development Plan:
----------------------------------
X A. Get location that we want to see photos of
X  1. use Geolocation API to get coordinates (lat and Xlon) or use a fallback location
 X   - [ this is a link to the documentation ]
X B. Get photo info from Flickr
  1. use fetch() to send a GET request to flickr.com/services/rest
    - Include the lat and lon
    - Include a search term
  2. Process the promises to get the photo data
    - Convert JSON to a usable object ("rehydrate")
    - Send the photo data to a display function
C. Display photos
  1. Create the image URLs from the photo data (function)
    - https://www.flickr.com/services/api/misc.urls.html
  2. Insert an <img> tag into the page
    - crate an img element
    - set src attribute to image URL
    - append the element to the right place in the document
  3. Display link to the image's Flickr page (function)
    - (Same stuff as the img tag)
D. Provide a way to advance through photos