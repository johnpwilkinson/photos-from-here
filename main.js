let lat;
let lon;
let photoArray = [];
let flickrUrl;
let i = 0;

let imgHolder = document.getElementById('imgHolder')

function geoLocActive() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(posReceived, posNotReceived);
  } else {
    alert("your browser is TRASH");
  }
}

function posReceived(position) {
  console.log(position);
  lat = position.coords.latitude;
  lon = position.coords.longitude;
  console.log(lat);
  console.log(lon);
  setTimeout(makeFlickrUrl, 1000)
}

function posNotReceived(error) {
  lat = 39;
  lon = -86;
}

//flickr api : 9016108e17b673704f50dae0be2ce717

// https://flickr.com/services/rest/?api_key=993fake589fake6cdfakefcb&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=39.76574&lon=-86.1579024&text=dog

function makeFlickrUrl() {
  let randyProxy = "https://shrouded-mountain-15003.herokuapp.com/";
  

flickrUrl =
    randyProxy +
    "https://flickr.com/services/rest/?" +
    "api_key=9016108e17b673704f50dae0be2ce717" +
    "&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&" +
    "lat=" +
    lat +
    "&" +
    "lon=" +
    lon +
    "&" +
    "text=dog";
  console.log(flickrUrl);




fetch(flickrUrl)
  .then(response => response.json())
  .then(data => {console.log(data);
    data.photos.photo.forEach(element => {
    let retPhoto = constructImageURL(element);
    photoArray.push(retPhoto);
    imgHolder.src = photoArray[i]  
    })
  })
}     

function constructImageURL(photoObj) {
  return (
    "https://farm" +
    photoObj.farm +
    ".staticflickr.com/" +
    photoObj.server +
    "/" +
    photoObj.id +
    "_" +
    photoObj.secret +
    ".jpg"
  );
  
}
imgHolder.src = photoArray[0]
window.addEventListener('keyup', presentImg)

function presentImg (event) {
  if (event.key == "Enter")
  geoLocActive();
  
}


let right = document.getElementById('right')
right.addEventListener('click', cycleImgRight)

function cycleImgRight (event) {
  console.log('right')
  if (i < 5){
  i += 1
  imgHolder.src = photoArray[i]
  }
  if(i > 4){
    imgHolder.src = photoArray[0]
    i = 0;
  }
}

/*
document.addEventListener('keyup', cycleImg)

function cycleImg (event) {
  if (event.key == 37 && i >= 1) {
    i--;
    console.log(photoArray[i])
    imgHolder.src = photoArray[i];
    console.log('left')
  }
  if (event.key == 39 ) {
    i++;
    console.log(photoArray[i])

    imgHolder.src = photoArray[i]
    console.log('right')
  }
}
*/